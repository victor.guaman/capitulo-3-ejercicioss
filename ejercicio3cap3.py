#Escribe un programa que solicite una puntuación entre 0.0 y 1.0. Si la
#puntuación está fuera de ese rango, muestra un mensaje de error. Si la puntuación
#está entre 0.0 y 1.0, muestra la calificación usando la tabla siguiente:
#nombre:VICTOR GUAMAN
try:

     puntuacion=float(input("introduzca la puntuacion "))
     if puntuacion >= 0 and puntuacion <= 1.0:
         if puntuacion >= 0.9:
                print("sobresaliente")
         elif puntuacion >= 0.8:
                print("notable")
         elif puntuacion >= 0.7:
                print("bien")
         elif puntuacion >= 0.6:
                print("suficiente")
         elif puntuacion <= 0.6:
                print("insuficiente")
     else:
        print("puntuacion incorrecta ")

except:
   print("puntuacion incorrecta ")

