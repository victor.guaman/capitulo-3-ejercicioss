#Ejercicio 2: Reescribe el programa del salario usando try y except, de modo que el
#programa sea capaz de gestionar entradas no numéricas con elegancia, mostrando
#un mensaje y saliendo del programa. A continuación se muestran dos ejecuciones
#del programa:
#nombre:VICTOR GUAMAN
try:
      hora=int(input("introduzca las horas: "))
      tarifa=float(input("introduzca la tarifa:"))

      if hora <= 40:
         salario=(hora*tarifa)
      if hora > 40:
         horaextra = hora - 40
         salario = (40 * tarifa) + (horaextra * 1.5 * tarifa)

         print(f"el salario bruto es:{salario}")

except:
         print("por favor ingrese un numero")
